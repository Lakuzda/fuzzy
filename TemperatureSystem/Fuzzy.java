

public class Fuzzy {
    //s = UserInterface.getExcTemp; // pobiera z klasy UserInterface wartosc temp oczekiwanej wpisanej przez uzytkownika
    String s = "18";

    public Fuzzy() {
    }

    public static void main(String[] args) {
        new Fuzzyfication();
        new Rules();
        new Inference();
        new Defuzzification();
    }

}

//---------------------------------------------------------------------------------------------------------------------//
// Rozmywanie
//---------------------------------------------------------------------------------------------------------------------//

class Fuzzyfication {

    public float[][] tem_dif = new float[3][100]; // tablica dla rozmytych wartości różnicy temperatur od 0 do 10 stopni
    public float[][] air_hum = new float[3][100]; //tablica dla rozmytych wartości wilgotności powietrza od 0 do 100%
    public float[][] heat_power = new float[3][100]; //tablica dla rozmytych wartośco mocy grzania od 0 do 100%

    void TemperatureDifference() {
        float low_tem_dif = 0; //wartość rozmyta dla niskiej różnicy temperatur (w tablicy [0][i], gdzie i jest różnicą temperatur)
        float medium_tem_dif = 0; //wartość rozmyta dla średniej różnicy temperatur (w tablicy [1][i], gdzie i jest różnicą temperatur)
        float high_tem_dif = 0; //wartość rozmyta dla wysokiej różnicy temperatur (w tablicy [2][i], gdzie i jest różnicą temperatur)
        float x; //zmienna pomocnicza

        for (int i = 0; i < 100; i++) { // rozmycie dla niskiej różnicy temperatur, wartości są umowane
            x = i;
            if (x <= 5)
                low_tem_dif = (x) / (5);                     //od 0 do 0.5 stopni waga stopnia rośnie
            else if ((x > 5) && (x <= 20))
                low_tem_dif = 1;                             //od 0.5 do 2 stopni waga stopnia wynosi 1
            else if ((x > 20) && (x <= 30))
                low_tem_dif = (30 - x) / (30 - 20);          //od 2 do 3 stopni waga stopnia maleje
            else
                low_tem_dif = 0;                             // dla 0 oraz powyżej 3 stopni waga stopnia wynosi 0
            tem_dif[0][i] = low_tem_dif;
            System.out.println(low_tem_dif);
        }

        for (int i = 0; i < 100; i++) {
            x = i;
            if ((x > 30) && (x <= 45))
                medium_tem_dif = (x - 30) / (45 - 30);
            else if ((x > 45) && (x <= 60))
                medium_tem_dif = 1;
            else if ((x > 60) && (x <= 75))
                medium_tem_dif = (75-x) / (75 - 60);
            else
                medium_tem_dif = 0;
            medium_tem_dif = medium_tem_dif * 100; //           zakrąglanie
            medium_tem_dif = Math.round(medium_tem_dif); //     do dwóch miejsc
            medium_tem_dif = medium_tem_dif / 100; //           po przecinku
            tem_dif[1][i] = medium_tem_dif;
        }

        for (int i = 0; i < 100; i++) {
            x = i;
            if ((x > 70) && (x <= 80))
                high_tem_dif = (x - 70) / (80 - 70);
            else if ((x > 80) && (x <= 90))
                high_tem_dif = 1;
            else if ((x > 90) && (x <= 100))
                high_tem_dif = (100 - x) / (100 - 90);
            else
                high_tem_dif = 0;
            tem_dif[2][i] = high_tem_dif;
        }
    }
    void AirHumidity() {
        float low_hum = 0; //wartość rozmyta dla niskiej wilgotnośći (w tablicy [0][i], gdzie i jest procentem)
        float medium_hum = 0; //wartość rozmyta dla średniej wilgotnośći (w tablicy [1][i], gdzie i jest procentem)
        float high_hum = 0; //wartość rozmyta dla wysokiej wilgotnośći (w tablicy [2][i], gdzie i jest procentem)
        float x; //zmienna pomocnicza

        for (int i = 0; i < 100; i++) {
            x = i;
            if (x <= 10)
                low_hum = (x) / (10);
            else if ((x > 10) && (x <= 30))
                low_hum = 1;
            else if ((x > 30) && (x <= 40))
                low_hum = (40 - x) / (40 - 30);
            else
                low_hum = 0;
            air_hum[0][i] = low_hum;
        }

        for (int i = 0; i < 100; i++) {
            x = i;
            if ((x > 30) && (x <= 45))
                medium_hum = (x - 30) / (45 - 30);
            else if ((x > 45) && (x <= 60))
                medium_hum = 1;
            else if ((x > 60) && (x <= 75))
                medium_hum = (75-x) / (75 - 60);
            else
                medium_hum = 0;
            medium_hum = medium_hum * 100; //           zakrąglanie
            medium_hum = Math.round(medium_hum); //     do dwóch miejsc
            medium_hum = medium_hum / 100; //           po przecinku
            air_hum[1][i] = medium_hum;
        }

        for (int i = 0; i < 100; i++) {
            x = i;
            if ((x > 70) && (x <= 80))
                high_hum = (x - 70) / (80 - 70);
            else if ((x > 80) && (x <= 90))
                high_hum = 1;
            else if ((x > 90) && (x <= 100))
                high_hum = (100 - x) / (100 - 90);
            else
                high_hum = 0;
            air_hum[2][i] = high_hum;
        }
    }
    void HeatPower() {
        float low_power = 0; //wartość rozmyta dla niskiej mocy grzania (w tablicy [0][i], gdzie i jest procentem)
        float medium_power = 0; //wartość rozmyta dla średniej mocy grzania (w tablicy [1][i], gdzie i jest procentem)
        float high_power = 0; //wartość rozmyta dla wysokiej mocy grzania (w tablicy [2][i], gdzie i jest procentem)
        float x; //zmienna pomocnicza

        for (int i = 0; i < 100; i++) {
            x = i;
            if (x <= 10)
                low_power = (x) / (10);
            else if ((x > 10) && (x <= 30))
                low_power = 1;
            else if ((x > 30) && (x <= 40))
                low_power = (40 - x) / (40 - 30);
            else
                low_power = 0;
            heat_power[0][i] = low_power;
        }

        for (int i = 0; i < 100; i++) {
            x = i;
            if ((x > 30) && (x <= 45))
                medium_power = (x - 30) / (45 - 30);
            else if ((x > 45) && (x <= 60))
                medium_power = 1;
            else if ((x > 60) && (x <= 75))
                medium_power = (75-x) / (75 - 60);
            else
                medium_power = 0;
            medium_power = medium_power * 100; //           zakrąglanie
            medium_power = Math.round(medium_power); //     do dwóch miejsc
            medium_power = medium_power / 100; //           po przecinku
            heat_power[1][i] = medium_power;
        }

        for (int i = 0; i < 100; i++) {
            x = i;
            if ((x > 70) && (x <= 80))
                high_power = (x - 70) / (80 - 70);
            else if ((x > 80) && (x <= 90))
                high_power = 1;
            else if ((x > 90) && (x <= 100))
                high_power = (100 - x) / (100 - 90);
            else
                high_power = 0;
            heat_power[2][i] = high_power;
        }
    }

}
//---------------------------------------------------------------------------------------------------------------------//
// Reguly
//---------------------------------------------------------------------------------------------------------------------//
class Rules extends Fuzzyfication {

    public Rules() {
        System.out.println("Klasa Rules...");
    }

    int w = 50; //tymczasowa zmienna wilgotność
    Fuzzy f = new Fuzzy();

    int tem =  Integer.parseInt(String.valueOf(f.s));
    int dif = tem - 12; // 12 jest tymczasową temperaturą odczytaną z termometru, by otrzymać różnicę temperatur

    //nasze reguły to
    //R1:IF Tem = Niska AND Wil = Średnia THEN Moc = Niska
    //R2:IF Tem = Średnia AND Wil = Niska THEN Moc = Średnia
    //R3:IF Tem = Wysoka AND Wil = Średnia THEN Moc = Średnia
    //R4:IF Tem = Wysoka AND Wil = Wysoka THEN Moc = Wysoka
    //Są to reguły przykładowe i można je dowolnie zmieniać
    private float R1, R2, R3, R4, R2_R3; //Jako że reguła 2 i 3 odpowiadają mocy średniej trzeba wybrać wartość maksymalną wyniku tych reguł

    void Rules() {
        R1 = Math.min(tem_dif[0][dif], air_hum[1][w]); //
        R2 = Math.min(tem_dif[1][dif], air_hum[0][w]); // Ustalanie wag reguł
        R3 = Math.min(tem_dif[2][dif], air_hum[1][w]); //
        R4 = Math.min(tem_dif[2][dif], air_hum[2][w]); //
        R2_R3 = Math.max(R2, R3);
    }
    public void HeatPowerRule(){
        for(int i = 0; i < 100; i++){
            if (R1 > heat_power[0][i])              // Przeniesienie
                heat_power[0][i] = R1;              // wag reguł
            if (R4 > heat_power[2][i])              // na
                heat_power[2][i] = R4;              // wagi
            if (R2_R3 > heat_power[1][i])           // mocy
                heat_power[1][i] = R2_R3;           // grzania
            System.out.println("heat power i reguly: " + heat_power[0][i]);
            System.out.println("heat power i reguly: " + heat_power[2][i]);
            System.out.println("heat power i reguly: " + heat_power[1][i]);
        }
    }

}

//---------------------------------------------------------------------------------------------------------------------//
// Wnioskowanie
//---------------------------------------------------------------------------------------------------------------------//

class Inference extends Rules {
    public float[] inf = new float[100]; // tablica dla rozmytych wartości różnicy temperatur od 0 do 10 stopni

    void Inference(){
        for (int i = 0; i < 100; i++){                                  //
            inf[i] = Math.max(heat_power[0][i], heat_power[1][i]);      // Wnioskowanie
            inf[i] = Math.max(inf[i], heat_power[2][i]);
            System.out.println("Inf i: " + inf[i]); //
        }

    }
}

//---------------------------------------------------------------------------------------------------------------------//
// Wyostrzanie
//---------------------------------------------------------------------------------------------------------------------//

class Defuzzification extends Inference {

    float def;      //wynik wyostrzania metodą środka ciężkości
    float a = 0;    //Licznik
    float b = 0;    //Mianownik

    public Defuzzification(){
        for (int i = 0; i < 100; i++) {
            a = (i * inf[i] + a);
            b = inf[i] + b;
        }
        def = a/b;
        System.out.println(def);
    }

}


