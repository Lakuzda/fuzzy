import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import jxl.Cell;
import jxl.CellType;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class ReadData {

    private String inputFile;
    public static String todayTemperature;
    public static String todayHumidity;

    public void setInputFile(String inputFile) {
        this.inputFile = inputFile;
    }

    // Czytanie wszystkich danych w arkuszu /xls
    public void read(int row, int column) throws IOException  {

        System.out.println("Wczytywanie danych...");
        //ReadData temperatures = new ReadData();
        this.setInputFile("temperatures.xls");
        //temperatures.read();

        File inputWorkbook = new File(inputFile);
        Workbook w;

        try {
            w = Workbook.getWorkbook(inputWorkbook);
            // Get the first sheet
            Sheet sheet = w.getSheet(0);

            // Odczyt wszystkich elementow

                    Cell celltemp = sheet.getCell(row, column);
                    Cell cellhum = sheet.getCell(row + 2, column);

                    // Temperatura
                        todayTemperature = celltemp.getContents(); // wartosc temperatury dla dnia obecnego
                        UserInterface.showTempLabel.setText(todayTemperature);
                        System.out.println("Row: " + celltemp.getRow() + ", today temperature: " + todayTemperature);

                    // Wilgotność
                        todayHumidity = cellhum.getContents(); // wartosc wilgotnosci dla dnia obecnego
                        System.out.println("Row: " + cellhum.getRow() + ", today humidity: " + todayHumidity);


        } catch (BiffException e) {
            e.printStackTrace();
        }

    } // koniec metody read()

    public static void main(String[] args) throws IOException {
        int x = 1;
        int y = 1;
        System.out.println("uruchomiono read data");
        ReadData temperatures = new ReadData();
        temperatures.setInputFile("temperatures.xls");
        temperatures.read(x, y);
        // System.out.println(todayTemperature + todayHumidity); test is okay
    }

}
